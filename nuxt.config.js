module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'NextSoccer - Sua escolinha de futebol online',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'NextSoccer - Sua escolinha de futebol online' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Source+Code+Pro:400,700' }
    ]
  },
  modules: [
    '@nuxtjs/bulma',
    '@nuxtjs/axios'
  ],
  plugins: [
    '~/plugins/vue-moment',
    '~/plugins/vue-youtube-embed'
  ],
  axios: {
    // proxyHeaders: false,
    browserBaseURL: 'https://nextsoccer-api.herokuapp.com/api',
    debug: true,
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#66A182' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['vue-moment', 'vue-youtube-embed'],
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
