export const state = () => ({
  all: [],
  selected: null,
})

export const mutations = {
  setCourses (state, data) {
    state.all = data;
  },
  setSelected (state, data) {
    state.selected = data;
  }
}
